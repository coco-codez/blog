---
title: "Vscode Remote Development Containers for Python"
date: 2019-06-16T23:02:08Z
tags: ["vscode", "python", "docker","containers"]
draft: false
---

# Vscode Remote Containers Tutorial for Python

This tutorial will walk through the basics of working with vscode remote - containers and python. By the end of this tutorial, you will have a full fledged remote development environment running inside a Docker container.

## Docker as a Development Environment

One of the biggest challenges for any developer or team is having a consistent development and testing environment. Docker provides a solution to this problem by isolating environments into containers. Until recently, actually developing inside docker containers while doable, was often a pain to setup and the overall experience left something to be desired. Enter [Visual Studio Code Remote - Containers](https://code.visualstudio.com/docs/remote/containers).

According to the docs:

 "Visual Studio Code Remote - Containers extension lets you use a Docker container as a full-featured development environment. It allows you to open any folder inside (or mounted into) a container and take advantage of VS Code's full feature set... This lets VS Code provide a local-quality development experience — including full IntelliSense (completions), code navigation, and debugging — regardless of where your tools (or code) is located"

 Let's see if it lives up to the hype.

### Setting Up Vscode

Visual Studio Code is an open source, cross-platform, lightweight but powerful IDE. Vscode's power comes from it rich extensions ecosystem, providing support for almost every major programming language and huge range of features. Visual Studio Code Remote - Containers is an extension that will allow us to develop inside a container but with all the convenience and features of a local development environment.

To get started, follow this [guide](https://code.visualstudio.com/docs/remote/containers#_getting-started) to get all the prerequistes installed and configured (vscode, docker, etc...). Make sure you have the May 2019 release or later, if not update to the latest version.

I highly recommend installing the [Docker](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker) extension. It allows you to interact with docker within vscode (I always love when I can avoid context switching).

### Setting up the Project

First, we're going to need some code to work with. We're going to build a simple Flask hello world app, using [poetry](https://poetry.eustace.io) to manage our application and development dependencies. Managing python environments can be a pain and add further complexity. By using docker we further isolate our environment and actually simplify things. There is a little bit of work to get it started, but this one time cost pays off because it allows anyone to start contributing to your project quickly.

We're also going to use the awesome [pre-commit](https://pre-commit.com/) library to enforce coding standards and style during development.

1. Clone the repo from [here](https://gitlab.com/cococodez/vscode-remote-containers-tutorial-for-python)

2. Cd into the directory and run `code .`

3. Follow the prompt to reopen the folder in a container
    ![alt text](/img/reopen-folder-in-container.PNG "reopen-folder-in-container")

That's it! Simple, straight forward and ready to start contributing!

### Understanding the .devcontainer

The `.devcontainer` directory is where all the magic happens. The presence of this directory with a `devcontainer.json` and a Dockerfile tells vscode to how to launch the remote development container. Let's break it down:

```json
"name": "Python 3",
"context": "..",
"dockerFile": "Dockerfile"
```

- `context`: The context folder to build the docker image from

- `dockerFile`: The docker file used to build the image of the container

**_Note_**: Both the Dockerfile and context folder are relative to the `devcontainer.json` path

```json
"appPort": [5000]
```

- `appPort`: Exposes the port of the container to the host

```json
"runArgs": [
    "-v","/var/run/docker.sock:/var/run/docker.sock",
    "-v", "~/.ssh:/tmp/.ssh"
    ]
```

- `runArgs`: Valid arguments of the `docker run` command, in this case mounting two volumes from the host into the container:

    1. Mounting the docker socket to the container to enable `dind`
    2. Copying the host ssh keys to the container so you can push to your repo from within the development container

```json
"postCreateCommand": "poetry install --no-interaction --no-ansi && pre-commit install && flask run --host=0.0.0.0"
```

- `postCreateCommand`: A command to run after creating the container. Here we install all our python dependencies with `poetry install`, install the pre-commit hooks and start the flask development server.

### The Dockerfile

Vscode remote containers have the ability to use an existing image, attach to a running container or build a container from a Dockerfile or docker-compose file. For this tutorial we're going to build a the remote development container from a Dockerfile. Microsoft has provided Dockerfiles for a broad range of tech stacks which you can use as a starting point. You can view the whole list [here](https://github.com/microsoft/vscode-dev-containers/tree/master/containers). To have vscode autogenerate the Dockerfile for you open the command pallette and run `Remote-Containers: Open Folder in Container`. This will take you through a series of prompts which will allow you to select from one of the default Dockerfiles to start with.

Let's take a look at our Dockerfile:

```dockerfile
FROM python:3.7

ENV PATH="${PATH}:/root/.poetry/bin"
```

We're using the official python 3.7 stable image, though I am not a fan of using stable images for the reasons documented [here](https://blogs.msdn.microsoft.com/stevelasker/2018/03/01/docker-tagging-best-practices-for-tagging-and-versioning-docker-images/). We're also adding poetry to the path.

```dockerfile
# Install git, process tools
RUN apt-get update && apt-get -y install git procps curl apt-transport-https \
    ca-certificates curl gnupg-agent software-properties-common lsb-release \
    && curl -fsSL https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]')/gpg | apt-key add - 2>/dev/null \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(lsb_release -is | tr '[:upper:]' '[:lower:]') $(lsb_release -cs) stable" \
    && apt-get update \
    && apt-get install -y docker-ce-cli \
    && curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python \
    && poetry completions bash > /etc/bash_completion.d/poetry.bash-completion \
    && poetry config settings.virtualenvs.create false
```

Next, we install any packages and run any installation commands such as installing git, curl and docker. I tend chain my `RUN` commands as a best practice but for a development container you could break these out into separate lines if you find yourself rebuilding the container often and want to make better use of layer caching to reduce build time.

```dockerfile
RUN mkdir /workspace
WORKDIR /workspace

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

# Set the default shell to bash instead of sh
ENV SHELL /bin/bash

# Set the Flask App to enable flask run
ENV FLASK_APP="/workspaces/vscode-remote-containers-tutorial-for-python/hello_world/app.py"

ENV FLASK_ENV=development
```

Finally, we clean up the container and set environment variables like the `FLASK_APP` and `FLASK_ENV`.

When launching the development container vscode will build the container from this Dockerfile and mount the workspace in the container in a volume and execute any additional commands and configurations as specified in our `.devcontainer.json` file.

### Why Go to All the Trouble ?

By now, we know the benefits of using containers for both build and run. If you're already doing development in containers you might be wondering why you should add another dependency, learn another build system and more `dot` files to an already cluttered repo. The answer is productivity. The remote container extension actually installs the vscode server inside the container and installs all the extension and settings too. This means all my favorite extensions still work, intellisense is enabled and the overall experience is no different than when developing on my local machine.

The biggest advantage is the extension does all this with little overhead and no changes to my normal workflow. I often forget I'm working in a container because the experience is that good.

### Room for improvement

Not everything is perfect, this is a very new extension and has a lot of potential to become even better. Here what I would like to see improved:

1. **Better defaults for `devcontainer.json`** - Things like mounting ssh keys, exposing the docker socket and other conviences "out of the box". Even if they are commented out at least the defaults would be there and wouldn't require adding them after the fact. Another approach would be to improve the interactive setup to ask these types of questions: _"Are you going need docker in docker?"_, _"Will you need access to you ssh keys to push to remote repositories?"_

2. **Multiple PostCreateCommands** - You can chain commands like `command && command` but if you have multiple commands you want to run the readability isn't great. It would be great if we could pass an array of commands

3. **Target Support for Multistage Dockerfiles** - Multistage Dockerfiles are commonplace now and are a recommended best practice to increase reusability and reduce image size. Adding the ability to specify a `--target` build arg would be a great feature and truly allow for one dockerfile for dev, test, build and run.

### Conclusions

The new remote containers extension makes development even easier while still giving all the benefits of working with vscode!
